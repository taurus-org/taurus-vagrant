Configuration necessary to build a Windows 10 VM foreseen for Taurus release
manual tests using Vagrant.

Requirements:
* Vagrant
* VM provider: libvirt or VirtualBox

Provisioned with:
* Python 3.8 (PyTango wheel is only available for this version)
* All other Taurus dependencies using pip

Basic usage (from within this project directory):
* to start VM `vagrant up` with either of this argument: `--provider=libvrit`
  or `--provider=virtualbox` (first time it will take some time to download
  the base box and this may take a lot of time, even more than 1 hour)
* to force provisioning: `vagrant provision` or `vagrant up --provision`
* to destroy VM: `vagrant destroy`
* to access the VM with a graphical interface: `virt-manager`

Testing Taurus (eventually some steps could be moved to the provisioning part):

0. Restart the VM in order to append Python3.8 and its Scripts to user Path.
1. Install Taurus e.g. `pip install taurus taurus_pyqtgraph`
2. Set `TANGO_HOST` environment variable e.g. to `controls05:10000`

## How to install libvirt on Debian 11 (bullseye):

Note: see the [Vagrant Debian wiki](https://wiki.debian.org/Vagrant) for details about how to set groups, etc.

Install Dependencies:

```
sudo apt install vagrant-libvirt libvirt-daemon-system
```

Install winrm plugins for Vagrant:
```
vagrant plugin install winrm winrm-fs winrm-elevated
```

Install virt-manager to graphically manage and access the VMs:
```
apt-get install virt-manager
```
